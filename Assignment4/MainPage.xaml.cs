﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Assignment4
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private User _user;

        private String _imgName;
        
        public MainPage()
        {
            this.InitializeComponent();

            _user = new User();

            LoadImage();
        }

        private void HumbergerButton_Click(object sender, RoutedEventArgs e)
        {
            SplitView1.IsPaneOpen = !SplitView1.IsPaneOpen;
        }

        private void MenuList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.MenuList.SelectedIndex == 2)
            {
                this.Frame.Navigate(typeof(AdminLogin));
            }

            else if (this.MenuList.SelectedIndex == 1)
            {
                this.Frame.Navigate(typeof(EditProfile));
            }
        }

        private void TextBlock_SelectionChanged(object sender, RoutedEventArgs e)
        {
            
        }

        private void _btnEdit_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(EditProfile));
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            _imgName = e.Parameter.ToString();
        }

        public async void LoadImage()
        {
            Windows.Storage.StorageFolder storageFolder = Windows.Storage.ApplicationData.Current.LocalFolder;
            Windows.Storage.StorageFile sampleFile = await storageFolder.CreateFileAsync($"image.txt", Windows.Storage.CreationCollisionOption.OpenIfExists);
            var fileStream = await sampleFile.OpenAsync(FileAccessMode.Read);
            var inputStream = fileStream.GetInputStreamAt(0);
            TextReader reader = new StreamReader(inputStream.AsStreamForRead());
            string name = reader.ReadLine();
            _image.Source = new BitmapImage(new Uri($@"C:\Users\Deep Patel\AppData\Local\Packages\70c41d6b-2903-435e-97bc-f6abe2f6e63f_js6h1s86dc6wc\LocalState\{name}", UriKind.RelativeOrAbsolute));
        }

        public async void UpdateInfo()
        {
            Windows.Storage.StorageFolder storageFolder = Windows.Storage.ApplicationData.Current.LocalFolder;
            Windows.Storage.StorageFile sampleFile = await storageFolder.CreateFileAsync("UserInfo.txt", Windows.Storage.CreationCollisionOption.OpenIfExists);
            var fileStream = await sampleFile.OpenAsync(FileAccessMode.Read);
            var inputStream = fileStream.GetInputStreamAt(0);
            TextReader reader = new StreamReader(inputStream.AsStreamForRead());
            string _userName = reader.ReadLine();
            string _userBirthday = reader.ReadLine();
            _txtName.Text = _userName;
            _txtBirthday.Text = _userBirthday;
        }
    }
}

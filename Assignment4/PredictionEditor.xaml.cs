﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Assignment4
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class PredictionEditor : Page
    {
        public PredictionEditor()
        {
            this.InitializeComponent();

        }

        private void HumbergerButtonPredictionEditor_Click(object sender, RoutedEventArgs e)
        {
            SplitView2.IsPaneOpen = !SplitView2.IsPaneOpen;
        }

        private void MenuList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void TextBlockPredictionEditor_SelectionChanged(object sender, RoutedEventArgs e)
        {

        }
    }
}

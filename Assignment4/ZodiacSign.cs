﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Media.Imaging;

namespace Assignment4
{
    public class ZodiacSign
    {
        private string _sign;

        private string _image;

        private string[] _predictions;
        public ZodiacSign(string sign)
        {
            _sign = sign;

            _predictions = new string[24];
        }

        public void DisplayInfo()
        {
            string[] signs = { "Aries", "Taurus", "Gemini","Cancer", "Leo", "Virgo",
                "Libra", "Scorpio", "Sagittarius", "Capricorn", "Aquarius","Pisces" };
            foreach (string element in signs)
            {
                if (_sign == element)
                {
                    DisplayImage();
                }
            }
            

        }
        public string DisplayImage()
        {
            Dictionary<string, string> zodiacImages = new Dictionary<string, string>
            {
                { "Aquarius", "aquarius.jpg"},
                { "Aries", "aries.jpg"},
                { "Taurus", "taurus.jpg"},
                { "Gemini", "gemini.jpg"},
                { "Pisces", "pisces.jpg"},
                { "Capricorn", "capricorn.jpg"},
                { "Sagittarius", "sagittarius.jpg"},
                { "Scorpio", "scorpio.jpg"},
                { "Libra", "libra.jpg"},
                { "Virgo", "virgo.jpg"},
                { "Leo", "leo.jpg"},
                { "Cancer", "aries.jpg"}
            };
            foreach(KeyValuePair<string,string> image in zodiacImages)
            {
                if(image.Key == _sign)
                {
                    return image.Value;
                }
                else
                {
                    return image.Key;
                }
               
            }
            return "hi";
        }
        public void GetListOfPredictions()
        {

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Assignment4
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class EditProfile : Page
    {
        private User _user;

        private string _imgName;

        public EditProfile()
        {
            this.InitializeComponent();
            LoadImage();
        }

        private void HumbergerButtonEditProfile_Click(object sender, RoutedEventArgs e)
        {
            SplitView3.IsPaneOpen = !SplitView3.IsPaneOpen;
        }

        private void MenuListEditProfile_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void TextBlock_SelectionChanged(object sender, RoutedEventArgs e)
        {

        }

        private void _btnApply_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(MainPage),_imgName);
        }

        public string Name
        {
            get
            {
                return _txtName.Text;
            }
        }

        public string Birthday
        {
            get
            {
                return _txtBirthday.ToString();
            }
        }

        public async void ImageButton_ClickAsync(object sender, RoutedEventArgs e)
        {
            FileOpenPicker openPicker = new FileOpenPicker();
            openPicker.ViewMode = PickerViewMode.Thumbnail;
            openPicker.SuggestedStartLocation = PickerLocationId.PicturesLibrary;
            openPicker.FileTypeFilter.Add(".jpg");
            openPicker.FileTypeFilter.Add(".jpeg");
            openPicker.FileTypeFilter.Add(".png");
            StorageFile file = await openPicker.PickSingleFileAsync();
            _imgName = file.Name;
            if (file != null)
            {
                var stream = await file.OpenAsync(FileAccessMode.Read);
                var bitmapImage = new BitmapImage();
                await bitmapImage.SetSourceAsync(stream);
                _image.Source = bitmapImage;
                var decoder = await Windows.Graphics.Imaging.BitmapDecoder.CreateAsync(stream);
                await file.CopyAsync(ApplicationData.Current.LocalFolder, _imgName, NameCollisionOption.ReplaceExisting);
                SaveImage();
            }
        }

        private void _calenderView_SelectedDatesChanged(CalendarView sender, CalendarViewSelectedDatesChangedEventArgs args)
        {

        }

        public async void SaveImage()
        {
            Windows.Storage.StorageFolder storageFolder = Windows.Storage.ApplicationData.Current.LocalFolder;
            Windows.Storage.StorageFile sampleFile = await storageFolder.CreateFileAsync($"image.txt", Windows.Storage.CreationCollisionOption.OpenIfExists);
            await Windows.Storage.FileIO.WriteTextAsync(sampleFile, _imgName);
        }

        public async void LoadImage()
        {
            Windows.Storage.StorageFolder storageFolder = Windows.Storage.ApplicationData.Current.LocalFolder;
            Windows.Storage.StorageFile sampleFile = await storageFolder.CreateFileAsync($"image.txt", Windows.Storage.CreationCollisionOption.OpenIfExists);
            var fileStream = await sampleFile.OpenAsync(FileAccessMode.Read);
            var inputStream = fileStream.GetInputStreamAt(0);
            TextReader reader = new StreamReader(inputStream.AsStreamForRead());
            string name = reader.ReadLine();
            _image.Source = new BitmapImage(new Uri($@"C:\Users\Deep Patel\AppData\Local\Packages\70c41d6b-2903-435e-97bc-f6abe2f6e63f_js6h1s86dc6wc\LocalState\{name}", UriKind.RelativeOrAbsolute));
        }

        public void SettingValues()
        {
            _user.Name = _txtName.Text;
            _user.Birthday = _txtBirthday.ToString();
            _user.SaveUserInfo();
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;

namespace Assignment4
{
    public class User
    {

        private string _name;

        private String _birthday;

        private Image _avatar;

        private string _userName;

        private string _userBirthday;

        public User()
        {
            //UpdateInfo();
        }

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public string Birthday
        {
            get
            {
                return _birthday;
            }
            set
            {
                _birthday = value;
            }
        }

        public string UserName
        {
            get
            {
                return _userName;
            }
        }

        public string UserBirthday
        {
            get
            {
                return _userBirthday;
            }
        }

        public async void SaveUserInfo()
        {
            List<string> userInfo = new List<string>();
            userInfo.Add(_name);
            userInfo.Add(_birthday.ToString());
            Windows.Storage.StorageFolder storageFolder = Windows.Storage.ApplicationData.Current.LocalFolder;
            Windows.Storage.StorageFile sampleFile = await storageFolder.CreateFileAsync("UserInfo.txt", Windows.Storage.CreationCollisionOption.ReplaceExisting);
            await Windows.Storage.FileIO.AppendLinesAsync(sampleFile, userInfo);
        }
    }
    
}
